\documentclass{IEEEconf}

\input{macros}

\author{Emmanuel Madrigal}

\title{Reverberation and its Effects on Spanish Speech Recognition}

\colorlet{punct}{red!60!black}
\definecolor{background}{HTML}{EEEEEE}
\definecolor{delim}{RGB}{20,105,176}
\colorlet{numb}{magenta!60!black}

\lstdefinelanguage{json}{
    basicstyle=\normalfont\ttfamily,
    numbers=left,
    numberstyle=\scriptsize,
    stepnumber=1,
    numbersep=8pt,
    showstringspaces=false,
    breaklines=true,
    frame=lines,
    backgroundcolor=\color{background},
    literate=
     *{0}{{{\color{numb}0}}}{1}
      {1}{{{\color{numb}1}}}{1}
      {2}{{{\color{numb}2}}}{1}
      {3}{{{\color{numb}3}}}{1}
      {4}{{{\color{numb}4}}}{1}
      {5}{{{\color{numb}5}}}{1}
      {6}{{{\color{numb}6}}}{1}
      {7}{{{\color{numb}7}}}{1}
      {8}{{{\color{numb}8}}}{1}
      {9}{{{\color{numb}9}}}{1}
      {:}{{{\color{punct}{:}}}}{1}
      {,}{{{\color{punct}{,}}}}{1}
      {\{}{{{\color{delim}{\{}}}}{1}
      {\}}{{{\color{delim}{\}}}}}{1}
      {[}{{{\color{delim}{[}}}}{1}
      {]}{{{\color{delim}{]}}}}{1},
}

\begin{document}

\maketitle

\begin{abstract}
This paper studies how to imitate acoustics of a room by registering its impulse
response. And also analysis the effects of reverberation on Spanish automatic
speech recognition systems.
\end{abstract}

\section{Introduction}

\subsection{Lineal Time-Invariant Systems}

Lineal and time invariant systems, also known as LTI, are useful tool in the
analysis of real fenomenon, since these allow the modeling of complex behaviors
using mathematical tool such as Fourier's and Laplace's transform
~\cite{alvarado-moya_2008}. In these systems, if its response $h(t)$ to an
impulse $\delta(t)$ is known, all other responses can be modeled by the
equation:

\begin{equation}
    y(t) = \int_{-\infty}^{\infty}x(\tau) h(t - \tau) d\tau = x(t) * h(t)
    \label{eq:freq_response}
\end{equation}

Using convolutions properties of Fourier's transform this equation can also be applied in the frequency domain:

\begin{equation}
    Y(j\omega) = \mathcal{F} \left\{ x(t) * h(t) \right\} = X(j \omega) H (j \omega)
\end{equation}

\subsection{Reverberation}

In most environments sound are accompanied by delayed
reflections from many different directions; rather than causing confusion our auditory system is able make them unnoticeable if these are occurring soon after the initial sound. Late reflections form a background ambience which is distinct from the foreground sound known as a reverberation~\cite{gardner2002reverberation}.

From a signal processing perspective, rooms containing sound sources and listeners can be considered as an LTI system. \footnote{Rooms are very linear but they are not time-invariant due to the motion of people and air. For practical purposes these are considered LTI systems~\cite{gardner2002reverberation}}

\subsection{Automatic Speech Recognition}

In the recent years, speech technology started to change the way we live and
work and became one of the primary means for humans to interact with some
devices. With the increased computational power available today (through multicore processors, and GPUs) and the greatly increased data for training of models, computational models have reduced the error rates of Automatic speech recognition systems (ASR by its initials)~\cite{Lu2020}. The typical architecture of an ASR system has four main components:

\begin{itemize}
    \item The signal processing and feature extraction component takes as input the audio signal, enhances the speech by removing noises and channel distortions, converts the signal from time-domain to frequency-domain, and extracts salient feature vectors that are suitable for the following acoustic models.
    \item The acoustic model integrates knowledge about acoustics and phonetics, takes as input the features generated from the feature extraction component, and generates an AM score for the variable-length feature sequence. 
    \item The language model estimates the probability of a hypothesized word sequence, or LM score, by learning the correlation between words from a (typically text) training corpora. The LM score often can be estimated more accurately if the prior knowledge about the domain or task is known. 
    \item The hypothesis search component combines AM and LM scores given the feature vector sequence and the hypothesized word sequence, and outputs the word sequence with the highest scores the recognition result.
\end{itemize}

\section{Methodology}

\subsection{Audio Captures}

Three audio samples will be captured:
\begin{itemize}
    \item A clean text with low environment noise.
    \item An impulse response in an environment with reverberation.
    \item The same text in the environment with reverberation.
\end{itemize}

\subsection{Reverberation Simulation}

The audio reverberation will be emulated using the clean text and the impulse response by applying equation \ref{eq:freq_response}. This is done in Octave using the following code:

\lstinputlisting[language=Matlab]{src/impulse_response_filter.m}

\subsection{Automatic Speech Recognition}

The capabilities of different 3 automatic speech recognition systems are compared, these using available online systems. The compared systems are:

\begin{itemize}
    \item Google's Speech-to-Text~\cite{google}
    \item Watson Speech to Text~\cite{ibm}
    \item Amazon Transcribe~\cite{amazon}
\end{itemize}

\section{Results}

The impulse response in the reverberated environment can be seen on figure~\ref{fig:impulse_response}. The reverberation effect can be seen since instead of having a single impulse at $t = 0$, several components can still be seen half a second later.

\begin{figure}[htpb]
    \centering
    \includegraphics[width=\linewidth]{figures/echo-impulse-response.pdf}
    \caption{Impulse response to a clap in a room with reverberation}
    \label{fig:impulse_response}
\end{figure}

\begin{figure}[htpb]
    \centering
    \includegraphics[width=\linewidth]{figures/standard-impulse-response.pdf}
    \caption{Standard Impulse response to a clap in a room with reverberation}
    \label{fig:standard_impulse_response}
\end{figure}

\begin{figure*}[htpb]
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics[width=\linewidth]{figures/clean_text.png}
		\caption{Original voice recording}
        \label{fig:clean_voice}
	\end{subfigure}
	\newline
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics[width=\linewidth]{figures/bathroom_text.png}
		\caption{Voice recording in room with reverberation}
        \label{fig:reverberated_voice}
	\end{subfigure}
	\newline
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics[width=\linewidth]{figures/octave_text.png}
		\caption{Original voice recording processed with the clap impulse response}
        \label{fig:simulated_reverberation}
	\end{subfigure}
	\newline
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics[width=\linewidth]{figures/standard_emulated.png}
		\caption{Original voice recording processed with the standard impulse response}
        \label{fig:simulated_standard}
	\end{subfigure}
    \caption{Different voice response}
    \label{fig:voice_recordings}
\end{figure*}

Figure~\ref{fig:clean_voice} shows the original voice recording, figure~\ref{fig:reverberated_voice} shows the voice in a room with reverberation. Finally figure~\ref{fig:simulated_reverberation} shows the original~\ref{fig:clean_voice} filtered with the~\ref{fig:impulse_response}.

Figure~\ref{fig:clean_voice} shows how in the clean environment each of the
phonemes is mostly separated. However once the reverberation effect has been
applied, these tend to be mixed and have a higher ``decay'', similar as what was
observed in figure~\ref{fig:impulse_response}.

By hearing this audio, the voice signal effectively has a reverb effect just
like it was captured in the bathroom. However in certain cases it has unwanted
effects on the audio, this probably because the impulse response is not perfect
and the reverberation room isn't a perfect LTI system.

Table~\ref{tab:word_error_rate} shows the error rates for different
speech-to-text systems against the different audios. Google's and AWS's have an
error rate of around 30\% for even the clean speech, while IBM's reaches almost
a 50\%, this can be most likely be attributed to a training dataset that doesn't
take into account different spanish accents, since the training database most
likely is skwed to accents from countries with lots of speakers. Also this
systems are mostly trained for english-speakers.

For the speech with reverberation Google's system is able to detected it mostly
without problems, while AWS's has almost double the amount of errors. For all
other cases all systems have errors in the 90\% range.

\begin{table*}[htpb]
    \centering
    \begin{tabular}{|l|l|l|l|}
    \hline
                                              & \textbf{Google (\%)} & \textbf{AWS (\%)} & \textbf{Watson IBM (\%)} \\ \hline
    \textbf{Clean Speech}                     & 32.5                 & 32.333            & 49.167                   \\ \hline
    \textbf{Speech with reverberation}          & 35.0                 & 61.667            & 96.667                   \\ \hline
    \textbf{Simulated Speech with reverberation} & 97.5                 & 95.833            & 96.667                   \\ \hline
    \textbf{Simulated Speech with ``standard'' reverberation} & 96.667                 & 87.5           & 98.333                   \\ \hline
    \end{tabular}
    \caption{Word error rate}
    \label{tab:word_error_rate}
\end{table*}

\section{Conclusions}

Rooms with reverberation can modeled as an LTI system, where the impulse
response can be used accurately simulated so that it sound perceptively close to
the human ear.

Speech recognition systems in Spanish don't achieve a 100\% accuracy recovering
the values from even on the clean text. Reverberation greatly affects, making it
impossible for the IBM's Speech-to-Text to distinguish the text. In all cases
the simulated reverberation still provides a worse response, which most likely
means that this reconstruction based from the impulse response isn't perfect.

\printbibliography

\FloatBarrier
\newpage

\section*{Appendix}

\subsection*{Audio files}

The used audio files can be found in the \texttt{data} folder. The description of the audio files are as follows:

\begin{itemize}
    \item \texttt{bathroom\_impulse\_response.wav}: Impulse response in room with reverberation.
    \item \texttt{bathroom\_text.wav}: Text read in the room with reverberation.
    \item \texttt{clean\_text.wav}: Text read in a room with low background noise.
    \item \texttt{octave\_text.wav}: Clean text processed with the room with reverberation impulse response.
    \item \texttt{standard\_impulse\_response.wav}: Standard impulse response
    \item \texttt{standard\_simulated.wav}: Clean text processed with the standard impulse response.
\end{itemize}

\subsection*{Speech to Text results}

\subsubsection*{Original Text}

Taken from~\cite{marquez2009cien}.

Muchos años después, frente al pelotón de fusilamiento, el coronel Aureliano
Buendía había de recordar aquella tarde remota en que su padre lo llevó a
conocer el hielo. Macondo era entonces una aldea de veinte casas de barro y
cañabrava construidas a la orilla de un río de aguas diáfanas que  se
precipitaban  por  un  lecho  de  piedras  pulidas,  blancas  y  enormes  como
huevos  prehistóricos.  El  mundo  era  tan  reciente,  que  muchas  cosas
carecían  de  nombre,  y  para  mencionarlas había que señalarías con el dedo.
Todos los años, por el mes de marzo, una familia de gitanos desarrapados
plantaba su carpa cerca de la aldea, y con un grande alboroto de pitos y
timbales daban a conocer los nuevos inventos.

\subsubsection*{Google}

\begin{itemize}
    \item Clean Text:

    muchos años después frente al pelotón de fusilamiento el coronel Aureliano Buendía había de recordar Aquella tarde remota en que su padre lo llevó a conocer el hielo macondo era entonces un entero 20 casas de barro y Caña Brava construidas a la orilla de un río de aguas diáfanas que se precipitaban por un lecho de Piedras pulidas Blancas y enormes como huevos prehistóricos el mundo del otro residente que muchas cosas carencias de nombre y para mencionarles Avianca señalan las con el dedo todos los años por el mes de marzo una familia de gitanos desarrapados plantado su carpa cerca de la aldea y con Gran Hotel protofitos y timbales dar a conocer los nuevos inventos

    \item Text with reverberation:

    Muchos años después frente al pelotón de fusilamiento el coronel Aureliano Buendía había de recordar Aquella tarde remota en que su padre lo llevó a conocer el diario bajo delantera de casas de barro y Caña Brava construidas a la orilla del río de aguas diáfanas que se precipitaban por un lecho de Piedras pulidas Blancas y enormes como huevos prehistóricos En dónde venden reciente que muchas cosas carecían de nombre y para mencionar a la sabía que tenían hablas con el dedo todos los años por el mes de marzo una familia de dictado Tezonapa 22 súper pacífica del día y cobran el boleto de pitos y timbal hasta conocernos de dos inventos

    \item Simulated text with reverberation:
    Buenos días de vacaciones y contraproducente para conocerlos

    \item Simulated text with ``standard'' reverberation:
    muchos años después de la tormenta película completa reciente conferencia de prensa coronavirus
    
\end{itemize}

\subsection*{Watson}
\begin{itemize}
    \item Clean Text:

    muchos años después frente al pelotón de fusilamiento el coronel aureliano buendía ya que debe cortar aquel plantel remotos en que su padre llegó a conocerlo cuenta con dos eventos uno el de veinte casas de barro y caña brava construidas a la orilla de buda la cual tienes que se precipitan por un lecho de piedras pulidas blancas y enormes como huevos prehistóricos el mundo es tan reciente que muchas cosas carecen de nombre y para mencionarlas hay que señalar con el dedo que todos los años por el mes de marzo una familia de gitanos de rapados plantados carpa cerca de la aldea cubran el prototipo sí timbales daba conocer los nuevos inventos

    \item Text with reverberation:

    muchos años después el conde y cómo hijos dejó entonces el haz algo es más es o es t es i es  dos años más tarde los ojos te jugaron varias

    \item Texto con reverberación simulado:
    y y veo tos la has las paces con jo es mas todos los las

    \item Simulated text with ``standard'' reverberation:
    muy bien qué qué qué qué qué qué qué qué todo lo que
\end{itemize}

\subsection*{AWS}

\begin{itemize}
    \item Clean Text:

    Muchos años después, frente al pelotón de fusilamiento, el coronel Aureliano Buendía hay de recordar aquella tarde remota en que su padre lo llevó a conocer. El hielo va cuando era entonces un el dedo, veinte casas de barbo y caña brava construidas a la orilla de un río de agua El te afanes que se precipitaban por el hecho de piedras pulidas, blancas y enormes como bosque, históricos El, Mundo, letras reciente que muchas cosas, carencias de nombre y para mí eso darles Había que señalarlos con el dedo todos los años. Por el mes de marzo, una familia de gitanos desarrapados plantaba su carpa cerca de la aldea y con gran altar Broto pitos y tema les da a conocer los dos inventos.

    \item Text with reverberation:

    Muchos años después frente al pelotón de fusilamiento, el Colón de Oro mediano. El día clave, el recuerdo de que yo tarde remota en que su padre de ocho a conocer el diez La fortuna. Entonces una aldea de casas de la lógica construidas a la orilla del río, de la cuantía Jaras que se precipita por un lecho de piedras pulidas, blancas y en Dolores con logos de históricos El Mundo de otros presidentes que muchas cosas que decía que no y para que, siendo erróneas e las cuentas todos los años por el mes de más una familia de Cartes agrupados plantadas para cercanamente y coger el alcohol auto de pito City para esta conocerlo un dos inventos.

    \item Simulated text with reverberation:

    ex-ante de borde por Viene muy bien, eh? Y padre, yo así Ha Le Havre a. M. Gracias. Sí, para la Voz de Asturias Le Havre Todos es poner Le Havre y De Esteban

    \item Simulated text with ``standard'' reverberation:
    Muchos años después tras treinta, pero todos los y las ciento veinticuatro en el porque ya no tienen ayer esta- que para el exterior de que que la co- encerrada en casa. Tema callara construida desde el brillo de graduación que se precipitaba a ser el de Molina de plata y co- por el punto de las gracias de que muchos con sus carencias en la Unión y para Per-Arne los otros esfuerzos de más m- termina ahí, en la desgajado trataba la carpa tercera de y comprar hasta por los potitos y pa- me

\end{itemize}

\subsection{}
Tabule o grafique los resultados, e integre el análisis de estos resultados en
el documento escrito, indicando aspectos como las limitaciones de los
procedimientos establecidos, y los posibles efectos en aplicaciones reales

\end{document}

