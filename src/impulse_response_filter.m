pkg load signal;

% Read input files
[x, fs] = audioread ("text.wav");
[h, fs] = audioread ("impulse.wav");

% Apply filtering
y = filter (h, 1, x);

% Normalize audio
y = y / max(y);

% Save file
audiowrite ("simulated.wav", y, fs);
